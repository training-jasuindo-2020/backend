create table audit_log (
    id varchar(36),
    activity_time timestamp not null,
    activity varchar(255) not null,
    username varchar(100) not null,
    primary key (id)
);