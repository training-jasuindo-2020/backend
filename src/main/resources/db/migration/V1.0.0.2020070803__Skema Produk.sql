create table product (
    id varchar(36),
    id_organization varchar(36) not null,
    code varchar(100) not null,
    name varchar(255) not null,
    primary key (id),
    foreign key (id_organization) references organization,
    unique (code)
);

create table batch (
    id varchar(36),
    id_product varchar(36),
    code varchar(100) not null,
    name varchar(255) not null,
    primary key (id),
    foreign key (id_product) references product
);