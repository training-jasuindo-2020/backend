INSERT INTO c_security_role (id, name, description) VALUES
('admin', 'Administrator', 'Admin App');

INSERT INTO c_security_permission (id, permission_label, permission_value) VALUES
('view_organization', 'view organization data', 'ORGANIZATION_VIEW');

INSERT INTO c_security_role_permission(id_role, id_permission) VALUES
('admin', 'view_organization');

INSERT INTO s_user (id, active, username, id_role) VALUES
('u001', true, 'user001', 'admin');

INSERT INTO s_user_password (id_user, password) VALUES
('u001', 'test123');