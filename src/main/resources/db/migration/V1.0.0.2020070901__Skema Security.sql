CREATE TABLE c_security_permission (
    id varchar(36) NOT NULL,
    permission_label varchar(100) NOT NULL,
    permission_value varchar(50) NOT NULL,
    primary key (id),
    unique (permission_label),
    unique (permission_value)
);

CREATE TABLE c_security_role (
     id varchar(36) NOT NULL,
     description varchar(255) NOT NULL,
     name varchar(255) NOT NULL,
     primary key (id),
     unique (name)
);

CREATE TABLE c_security_role_permission (
    id_role varchar(36) NOT NULL,
    id_permission varchar(36) NOT NULL,
    primary key (id_role, id_permission),
    foreign key (id_role) references c_security_role(id),
    foreign key (id_permission) references c_security_permission(id)
);

CREATE TABLE s_user (
    id varchar(36) NOT NULL,
    active boolean NOT NULL,
    username varchar(255) NOT NULL,
    id_role varchar(36) NOT NULL,
    primary key (id),
    unique (username),
    foreign key (id_role) references c_security_role(id)
);

CREATE TABLE s_user_password (
     id_user varchar(36) NOT NULL,
     password varchar(255) NOT NULL,
     primary key (id_user),
     foreign key (id_user) references s_user(id)
);
