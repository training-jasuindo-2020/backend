alter table organization
add column id_parent varchar(36);

alter table organization
add foreign key (id_parent) references organization;