package id.artivisi.training.jasuindo.backend.dto;

public interface JumlahBatchOrganisasi {
     String getKodeOrganisasi();
     String getNamaOrganisasi();
     Integer getJumlahBatch();
}
