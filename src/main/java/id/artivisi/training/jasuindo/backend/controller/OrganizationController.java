package id.artivisi.training.jasuindo.backend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.artivisi.training.jasuindo.backend.dao.OrganizationDao;
import id.artivisi.training.jasuindo.backend.entity.Organization;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller @Slf4j
public class OrganizationController {

    @Autowired
    private OrganizationDao organizationDao;

    @Autowired private ObjectMapper objectMapper;

    @PostMapping("/api/organization/")
    @ResponseStatus(HttpStatus.CREATED)
    public void simpan(@RequestBody @Valid Organization organization) {
        organizationDao.save(organization);
    }

    @GetMapping("/api/organization/")
    @ResponseBody
    public Iterable<Organization> semua(Authentication currentUser) throws Exception {
        String infoUser = objectMapper.writeValueAsString(currentUser);
        log.info("User yang sedang login : {}", infoUser);
        String username = ((Jwt) currentUser.getPrincipal()).getClaim("user_name");
        log.info("Username : {}", username);
        return organizationDao.findAll();
    }

    @GetMapping("/api/organization/search")
    public ResponseEntity<Organization> cariByKode(@RequestParam String code) {
        Organization org = organizationDao.findByCode(code);
        if (org == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(org);
    }

    @GetMapping("/api/organization/{id}")
    @ResponseBody
    public Organization findById(@PathVariable String id) {
        return organizationDao.findById(id).get();
    }

    @PutMapping("/api/organization/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void findById(@PathVariable String id, @RequestBody @Valid Organization organization) {
        organizationDao.findById(id)
                .ifPresent(o -> {
                    organization.setId(id);
                    organizationDao.save(organization);
                });
    }
}
