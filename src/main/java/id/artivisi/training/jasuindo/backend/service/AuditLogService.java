package id.artivisi.training.jasuindo.backend.service;

import id.artivisi.training.jasuindo.backend.dao.AuditLogDao;
import id.artivisi.training.jasuindo.backend.entity.AuditLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service @Transactional(propagation = Propagation.REQUIRES_NEW)
public class AuditLogService {
    @Autowired private AuditLogDao auditLogDao;

    public void save(AuditLog auditLog) {
        auditLogDao.save(auditLog);
    }
}
