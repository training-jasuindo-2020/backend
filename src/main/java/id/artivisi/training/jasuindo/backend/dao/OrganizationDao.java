package id.artivisi.training.jasuindo.backend.dao;

import id.artivisi.training.jasuindo.backend.entity.Organization;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrganizationDao
        extends PagingAndSortingRepository<Organization, String> {
    Organization findByCode(String code);
}
