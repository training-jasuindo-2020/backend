package id.artivisi.training.jasuindo.backend.dao;

import id.artivisi.training.jasuindo.backend.dto.JumlahBatchOrganisasi;
import id.artivisi.training.jasuindo.backend.dto.JumlahBatchProduk;
import id.artivisi.training.jasuindo.backend.entity.Batch;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BatchDao extends PagingAndSortingRepository<Batch, String> {
    Iterable<Batch> findByProductCode(String code);
    Iterable<Batch> findByProductOrganizationCode(String code);

    @Query(
            value = "select o.code as kodeOrganisasi, o.name as namaOrganisasi, count(b.id) as jumlahBatch " +
                    "from batch b " +
                    "inner join product p on b.id_product = p.id " +
                    "inner join organization o on p.id_organization = o.id " +
                    "group by (o.id)"
            , nativeQuery = true
    )
    Iterable<JumlahBatchOrganisasi> hitungJumlahBatchPerOrganisasi();

    @Query(
            value = "select new id.artivisi.training.jasuindo.backend.dto.JumlahBatchProduk(b.product.code, b.product.name, count(b.id) ) " +
                    "from Batch b " +
                    "group by b.product.code, b.product.name"
    )
    Iterable<JumlahBatchProduk> hitungJumlahBatchPerProduk();
}
