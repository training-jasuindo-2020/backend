package id.artivisi.training.jasuindo.backend.dao;

import id.artivisi.training.jasuindo.backend.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {
}
