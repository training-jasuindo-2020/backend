package id.artivisi.training.jasuindo.backend.service;

import id.artivisi.training.jasuindo.backend.dao.BatchDao;
import id.artivisi.training.jasuindo.backend.dao.OrganizationDao;
import id.artivisi.training.jasuindo.backend.dao.ProductDao;
import id.artivisi.training.jasuindo.backend.entity.Batch;
import id.artivisi.training.jasuindo.backend.entity.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service @Transactional
public class BackendService {
    @Autowired
    private BatchDao batchDao;
    @Autowired private ProductDao productDao;
    @Autowired private OrganizationDao organizationDao;

    public void save(Batch batch) {
        String organizationId = batch.getProduct().getOrganization().getId();
        if (organizationId == null || !organizationDao.existsById(organizationId)) {
            organizationDao.save(batch.getProduct().getOrganization());
        }

        String productId = batch.getProduct().getId();
        if (productId == null || !productDao.existsById(productId)) {
            productDao.save(batch.getProduct());
        }

        if ("B-99".equals(batch.getCode())) {
            throw new IllegalStateException("simulasi error");
        }
        batchDao.save(batch);
    }
}
