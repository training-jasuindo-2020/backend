package id.artivisi.training.jasuindo.backend.controller;

import id.artivisi.training.jasuindo.backend.dao.AuditLogDao;
import id.artivisi.training.jasuindo.backend.dao.BatchDao;
import id.artivisi.training.jasuindo.backend.dao.OrganizationDao;
import id.artivisi.training.jasuindo.backend.dao.ProductDao;
import id.artivisi.training.jasuindo.backend.dto.JumlahBatchOrganisasi;
import id.artivisi.training.jasuindo.backend.dto.JumlahBatchProduk;
import id.artivisi.training.jasuindo.backend.entity.AuditLog;
import id.artivisi.training.jasuindo.backend.entity.Batch;
import id.artivisi.training.jasuindo.backend.service.AuditLogService;
import id.artivisi.training.jasuindo.backend.service.BackendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/api/batch")
public class BatchController {

    @Autowired private BackendService backendService;
    @Autowired private BatchDao batchDao;
    @Autowired private AuditLogService auditLogService;

    @GetMapping("/")
    public Iterable<Batch> semuaBatch() {
        return batchDao.findAll();
    }

    @GetMapping("/product/{code}")
    public Iterable<Batch> cariByKodeProduk(@PathVariable String code) {
        return batchDao.findByProductCode(code);
    }

    @GetMapping("/organization/{code}")
    public Iterable<Batch> cariByKodeOrganisasi(@PathVariable String code) {
        return batchDao.findByProductOrganizationCode(code);
    }

    @GetMapping("/rekap_organisasi")
    public Iterable<JumlahBatchOrganisasi> hitungJumlahBatchOrganisasi() {
        return batchDao.hitungJumlahBatchPerOrganisasi();
    }

    @GetMapping("/rekap_produk")
    public Iterable<JumlahBatchProduk> hitungJumlahBatchProduk() {
        return batchDao.hitungJumlahBatchPerProduk();
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public void simpanBatch(@RequestBody Batch batch, Authentication currentUser) {
        String username = ((Jwt) currentUser.getPrincipal()).getClaim("user_name");
        AuditLog log = new AuditLog();
        log.setUsername(username);
        log.setActivity("Save Batch");
        log.setActivityTime(LocalDateTime.now());
        auditLogService.save(log);
        
        backendService.save(batch);


    }
}
