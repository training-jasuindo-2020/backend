package id.artivisi.training.jasuindo.backend.dao;

import id.artivisi.training.jasuindo.backend.entity.AuditLog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRES_NEW)
public interface AuditLogDao extends CrudRepository<AuditLog, String> {
}
