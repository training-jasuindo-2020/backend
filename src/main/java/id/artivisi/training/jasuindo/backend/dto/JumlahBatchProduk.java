package id.artivisi.training.jasuindo.backend.dto;

import lombok.Data;

@Data
public class JumlahBatchProduk {
    private String kodeProduk;
    private String namaProduk;
    private Long jumlahBatch;

    public JumlahBatchProduk() {
    }

    public JumlahBatchProduk(String kodeProduk, String namaProduk, Long jumlahBatch) {
        this.kodeProduk = kodeProduk;
        this.namaProduk = namaProduk;
        this.jumlahBatch = jumlahBatch;
    }
}
