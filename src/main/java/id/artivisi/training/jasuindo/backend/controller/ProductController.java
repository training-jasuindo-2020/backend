package id.artivisi.training.jasuindo.backend.controller;

import id.artivisi.training.jasuindo.backend.dao.ProductDao;
import id.artivisi.training.jasuindo.backend.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired private ProductDao productDao;

    @Transactional
    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public void bulkInsertProduct(@RequestBody List<Product> dataProduk){

        for (Product p : dataProduk) {
            productDao.save(p);
            if ("P-103".equals(p.getCode())) {
                throw new IllegalStateException("simulasi error");
            }
        }

        /*
        productDao.saveAll(dataProduk);
        if (dataProduk.size() > 3) {
            throw new IllegalStateException("simulasi error");
        }
         */

    }

    @GetMapping("/")
    public Iterable<Product> semuaProduk() {
        return productDao.findAll();
    }
}
