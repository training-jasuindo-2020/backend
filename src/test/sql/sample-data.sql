delete from batch;
delete from product;
delete from organization;

insert into organization (id, code, name)
values ('o001', 'O-001', 'Organization 001');

insert into organization (id, code, name, id_parent)
values ('o002', 'O-002', 'Organization 002', 'o001');

insert into product (id, id_organization, code, name)
values ('p001', 'o001', 'P-001', 'Product 001');

insert into product (id, id_organization, code, name)
values ('p002', 'o002', 'P-002', 'Product 002');

insert into product (id, id_organization, code, name)
values ('p003', 'o001', 'P-003', 'Product 003');

insert into batch (id, id_product, code, name)
values ('b001', 'p001', 'B-001', 'Batch 001');

insert into batch (id, id_product, code, name)
values ('b002', 'p002', 'B-002', 'Batch 002');

insert into batch (id, id_product, code, name)
values ('b003', 'p001', 'B-003', 'Batch 003');