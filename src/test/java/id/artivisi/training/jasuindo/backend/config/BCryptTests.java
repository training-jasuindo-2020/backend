package id.artivisi.training.jasuindo.backend.config;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
public class BCryptTests {

    @Autowired private PasswordEncoder passwordEncoder;

    @Test
    public void testHashPassword() {
        String password = "test123";
        String hashed = passwordEncoder.encode(password);
        System.out.println("Password plain : " + password);
        System.out.println("Password hash : " + hashed);
    }
}
